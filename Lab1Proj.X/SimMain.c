
//#include "ME218_PIC32_2020.h"
#include <xc.h>
#include "portmap.h"

/*--------------------------------------
 test harness function to test pinMode()
 and digitalRead()
 --------------------------------------*/

#define TSW                 PORTAbits.RA0 
//mode pin to allow testing and force test harness to proceed to next pin


int main(void) 
{
    //Configure all ports to digital
    digiPin();
    
    /*-----------------------------------
     * Configure all ports to inputs in
     * order to test functions
     * ---------------------------------*/
    TRISA = 1;
    TRISB = 1;
    pinMode(RB15,0);
    pinMode(RA0,1);    //port RB15 set to output to allow measurement changes
                       //when manipulating variables; RA0 is input for switch
    //Infinite loop since microprocessor cannot return anywhere
    while(1)
    {
        /*-------------------------------------------------------------------
         Write 0 to all ports to begin event checking event with known value
         and test digital read and digital write functions
         -------------------------------------------------------------------*/
        LATA = 0;
        LATB = 0;
        //Event checking for loop variable declaration
        static int i = 2;
        static int j;
        
        while (i <= 20)
        {
            //Variable declaration to ensure input and output are equal
            
            //Implement digitalWrite() and set value to high, allowing to see a difference
            //from initialization value of all ports
            

                LATBbits.LATB15=digitalRead(i);

                if (TSW == 1)
                {
                    i++; //touch sensor input breaks from loop and increment i 
                    // to proceed to next pin
                    for(j=1;j<1000000;j++){}
                }

        }
    }

}



