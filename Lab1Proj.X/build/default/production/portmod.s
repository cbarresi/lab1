	.section .mdebug.abi32
	.previous
	.gnu_attribute 4, 3
	.section	.text,code
.Ltext0:
	.align	2
	.globl	digiPin
.LFB7 = .
	.file 1 "c:/users/chris/documents/stanford university/trimester classes/20-21 autuum/me 218a/lab/lab 1/code/lab1proj.x/portmod.c"
	.loc 1 7 0
	.set	nomips16
	.set	nomicromips
	.ent	digiPin
	.type	digiPin, @function
digiPin:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
# End mchp_output_function_prologue
	addiu	$sp,$sp,-8
.LCFI0 = .
	sw	$fp,4($sp)
.LCFI1 = .
	move	$fp,$sp
.LCFI2 = .
	.loc 1 8 0
	lui	$2,%hi(ANSELA)
	sw	$0,%lo(ANSELA)($2)
	.loc 1 9 0
	lui	$2,%hi(ANSELB)
	sw	$0,%lo(ANSELB)($2)
	.loc 1 10 0
	move	$sp,$fp
.LCFI3 = .
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
.LCFI4 = .
	j	$31
	nop

	.set	macro
	.set	reorder
# Begin mchp_output_function_epilogue
# End mchp_output_function_epilogue
	.end	digiPin
.LFE7:
	.size	digiPin, .-digiPin
	.align	2
	.globl	pinMode
.LFB8 = .
	.loc 1 23 0
	.set	nomips16
	.set	nomicromips
	.ent	pinMode
	.type	pinMode, @function
pinMode:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
# End mchp_output_function_prologue
	addiu	$sp,$sp,-8
.LCFI5 = .
	sw	$fp,4($sp)
.LCFI6 = .
	move	$fp,$sp
.LCFI7 = .
	sw	$4,8($fp)
	sw	$5,12($fp)
	.loc 1 25 0
	lw	$2,8($fp)
	sltu	$2,$2,22
	beq	$2,$0,.L3
	nop

	lw	$2,8($fp)
	sll	$3,$2,2
	lui	$2,%hi(.L5)
	addiu	$2,$2,%lo(.L5)
	addu	$2,$3,$2
	lw	$2,0($2)
	j	$2
	nop

	.align	2
	.align	2
.L5:
	.word	.L3
	.word	.L4
	.word	.L6
	.word	.L7
	.word	.L8
	.word	.L9
	.word	.L10
	.word	.L11
	.word	.L12
	.word	.L13
	.word	.L14
	.word	.L15
	.word	.L16
	.word	.L17
	.word	.L18
	.word	.L19
	.word	.L20
	.word	.L21
	.word	.L22
	.word	.L23
	.word	.L24
	.word	.L25
.L4:
	.loc 1 30 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L26
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L27
	nop

.L26:
	.loc 1 32 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISA)
	lbu	$2,%lo(TRISA)($3)
	ins	$2,$4,0,1
	sb	$2,%lo(TRISA)($3)
	.loc 1 33 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L27:
	.loc 1 37 0
	move	$2,$0
	j	.L28
	nop

.L6:
	.loc 1 43 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L29
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L30
	nop

.L29:
	.loc 1 45 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISA)
	lbu	$2,%lo(TRISA)($3)
	ins	$2,$4,1,1
	sb	$2,%lo(TRISA)($3)
	.loc 1 46 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L30:
	.loc 1 50 0
	move	$2,$0
	j	.L28
	nop

.L7:
	.loc 1 56 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L31
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L32
	nop

.L31:
	.loc 1 58 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISA)
	lbu	$2,%lo(TRISA)($3)
	ins	$2,$4,2,1
	sb	$2,%lo(TRISA)($3)
	.loc 1 59 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L32:
	.loc 1 63 0
	move	$2,$0
	j	.L28
	nop

.L8:
	.loc 1 69 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L33
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L34
	nop

.L33:
	.loc 1 71 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISA)
	lbu	$2,%lo(TRISA)($3)
	ins	$2,$4,3,1
	sb	$2,%lo(TRISA)($3)
	.loc 1 72 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L34:
	.loc 1 76 0
	move	$2,$0
	j	.L28
	nop

.L9:
	.loc 1 82 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L35
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L36
	nop

.L35:
	.loc 1 84 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISA)
	lbu	$2,%lo(TRISA)($3)
	ins	$2,$4,4,1
	sb	$2,%lo(TRISA)($3)
	.loc 1 85 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L36:
	.loc 1 89 0
	move	$2,$0
	j	.L28
	nop

.L10:
	.loc 1 95 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L37
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L38
	nop

.L37:
	.loc 1 97 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,0,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 98 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L38:
	.loc 1 102 0
	move	$2,$0
	j	.L28
	nop

.L11:
	.loc 1 108 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L39
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L40
	nop

.L39:
	.loc 1 110 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,1,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 111 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L40:
	.loc 1 115 0
	move	$2,$0
	j	.L28
	nop

.L12:
	.loc 1 121 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L41
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L42
	nop

.L41:
	.loc 1 123 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,2,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 124 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L42:
	.loc 1 128 0
	move	$2,$0
	j	.L28
	nop

.L13:
	.loc 1 134 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L43
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L44
	nop

.L43:
	.loc 1 136 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,3,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 137 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L44:
	.loc 1 141 0
	move	$2,$0
	j	.L28
	nop

.L14:
	.loc 1 147 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L45
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L46
	nop

.L45:
	.loc 1 149 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,4,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 150 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L46:
	.loc 1 154 0
	move	$2,$0
	j	.L28
	nop

.L15:
	.loc 1 160 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L47
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L48
	nop

.L47:
	.loc 1 162 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,5,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 163 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L48:
	.loc 1 167 0
	move	$2,$0
	j	.L28
	nop

.L16:
	.loc 1 173 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L49
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L50
	nop

.L49:
	.loc 1 175 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,6,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 176 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L50:
	.loc 1 180 0
	move	$2,$0
	j	.L28
	nop

.L17:
	.loc 1 186 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L51
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L52
	nop

.L51:
	.loc 1 188 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,7,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 189 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L52:
	.loc 1 193 0
	move	$2,$0
	j	.L28
	nop

.L18:
	.loc 1 199 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L53
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L54
	nop

.L53:
	.loc 1 201 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,8,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 202 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L54:
	.loc 1 206 0
	move	$2,$0
	j	.L28
	nop

.L19:
	.loc 1 212 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L55
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L56
	nop

.L55:
	.loc 1 214 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,9,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 215 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L56:
	.loc 1 219 0
	move	$2,$0
	j	.L28
	nop

.L20:
	.loc 1 225 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L57
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L58
	nop

.L57:
	.loc 1 227 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,10,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 228 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L58:
	.loc 1 232 0
	move	$2,$0
	j	.L28
	nop

.L21:
	.loc 1 238 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L59
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L60
	nop

.L59:
	.loc 1 240 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,11,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 241 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L60:
	.loc 1 245 0
	move	$2,$0
	j	.L28
	nop

.L22:
	.loc 1 251 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L61
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L62
	nop

.L61:
	.loc 1 253 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,12,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 254 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L62:
	.loc 1 258 0
	move	$2,$0
	j	.L28
	nop

.L23:
	.loc 1 264 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L63
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L64
	nop

.L63:
	.loc 1 266 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,13,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 267 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L64:
	.loc 1 271 0
	move	$2,$0
	j	.L28
	nop

.L24:
	.loc 1 277 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L65
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L66
	nop

.L65:
	.loc 1 279 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,14,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 280 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L66:
	.loc 1 284 0
	move	$2,$0
	j	.L28
	nop

.L25:
	.loc 1 290 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L67
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L68
	nop

.L67:
	.loc 1 292 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(TRISB)
	lhu	$2,%lo(TRISB)($3)
	ins	$2,$4,15,1
	sh	$2,%lo(TRISB)($3)
	.loc 1 293 0
	li	$2,1			# 0x1
	j	.L28
	nop

.L68:
	.loc 1 297 0
	move	$2,$0
	j	.L28
	nop

.L3:
	.loc 1 301 0
	move	$2,$0
.L28:
	.loc 1 304 0
	move	$sp,$fp
.LCFI8 = .
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
.LCFI9 = .
	j	$31
	nop

	.set	macro
	.set	reorder
# Begin mchp_output_function_epilogue
# End mchp_output_function_epilogue
	.end	pinMode
.LFE8:
	.size	pinMode, .-pinMode
	.align	2
	.globl	digitalWrite
.LFB9 = .
	.loc 1 312 0
	.set	nomips16
	.set	nomicromips
	.ent	digitalWrite
	.type	digitalWrite, @function
digitalWrite:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
# End mchp_output_function_prologue
	addiu	$sp,$sp,-8
.LCFI10 = .
	sw	$fp,4($sp)
.LCFI11 = .
	move	$fp,$sp
.LCFI12 = .
	sw	$4,8($fp)
	sw	$5,12($fp)
	.loc 1 314 0
	lw	$2,8($fp)
	sltu	$2,$2,22
	beq	$2,$0,.L70
	nop

	lw	$2,8($fp)
	sll	$3,$2,2
	lui	$2,%hi(.L72)
	addiu	$2,$2,%lo(.L72)
	addu	$2,$3,$2
	lw	$2,0($2)
	j	$2
	nop

	.align	2
	.align	2
.L72:
	.word	.L70
	.word	.L71
	.word	.L73
	.word	.L74
	.word	.L75
	.word	.L76
	.word	.L77
	.word	.L78
	.word	.L79
	.word	.L80
	.word	.L81
	.word	.L82
	.word	.L83
	.word	.L84
	.word	.L85
	.word	.L86
	.word	.L87
	.word	.L88
	.word	.L89
	.word	.L90
	.word	.L91
	.word	.L92
.L71:
	.loc 1 319 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L93
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L94
	nop

.L93:
	.loc 1 321 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATA)
	lbu	$2,%lo(LATA)($3)
	ins	$2,$4,0,1
	sb	$2,%lo(LATA)($3)
	.loc 1 322 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L94:
	.loc 1 326 0
	move	$2,$0
	j	.L95
	nop

.L73:
	.loc 1 332 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L96
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L97
	nop

.L96:
	.loc 1 334 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATA)
	lbu	$2,%lo(LATA)($3)
	ins	$2,$4,1,1
	sb	$2,%lo(LATA)($3)
	.loc 1 335 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L97:
	.loc 1 339 0
	move	$2,$0
	j	.L95
	nop

.L74:
	.loc 1 345 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L98
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L99
	nop

.L98:
	.loc 1 347 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATA)
	lbu	$2,%lo(LATA)($3)
	ins	$2,$4,2,1
	sb	$2,%lo(LATA)($3)
	.loc 1 348 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L99:
	.loc 1 352 0
	move	$2,$0
	j	.L95
	nop

.L75:
	.loc 1 358 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L100
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L101
	nop

.L100:
	.loc 1 360 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATA)
	lbu	$2,%lo(LATA)($3)
	ins	$2,$4,3,1
	sb	$2,%lo(LATA)($3)
	.loc 1 361 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L101:
	.loc 1 365 0
	move	$2,$0
	j	.L95
	nop

.L76:
	.loc 1 371 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L102
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L103
	nop

.L102:
	.loc 1 373 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATA)
	lbu	$2,%lo(LATA)($3)
	ins	$2,$4,4,1
	sb	$2,%lo(LATA)($3)
	.loc 1 374 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L103:
	.loc 1 378 0
	move	$2,$0
	j	.L95
	nop

.L77:
	.loc 1 384 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L104
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L105
	nop

.L104:
	.loc 1 386 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,0,1
	sh	$2,%lo(LATB)($3)
	.loc 1 387 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L105:
	.loc 1 391 0
	move	$2,$0
	j	.L95
	nop

.L78:
	.loc 1 397 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L106
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L107
	nop

.L106:
	.loc 1 399 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,1,1
	sh	$2,%lo(LATB)($3)
	.loc 1 400 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L107:
	.loc 1 404 0
	move	$2,$0
	j	.L95
	nop

.L79:
	.loc 1 410 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L108
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L109
	nop

.L108:
	.loc 1 412 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,2,1
	sh	$2,%lo(LATB)($3)
	.loc 1 413 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L109:
	.loc 1 417 0
	move	$2,$0
	j	.L95
	nop

.L80:
	.loc 1 423 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L110
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L111
	nop

.L110:
	.loc 1 425 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,3,1
	sh	$2,%lo(LATB)($3)
	.loc 1 426 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L111:
	.loc 1 430 0
	move	$2,$0
	j	.L95
	nop

.L81:
	.loc 1 436 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L112
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L113
	nop

.L112:
	.loc 1 438 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,4,1
	sh	$2,%lo(LATB)($3)
	.loc 1 439 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L113:
	.loc 1 443 0
	move	$2,$0
	j	.L95
	nop

.L82:
	.loc 1 449 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L114
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L115
	nop

.L114:
	.loc 1 451 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,5,1
	sh	$2,%lo(LATB)($3)
	.loc 1 452 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L115:
	.loc 1 456 0
	move	$2,$0
	j	.L95
	nop

.L83:
	.loc 1 462 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L116
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L117
	nop

.L116:
	.loc 1 464 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,6,1
	sh	$2,%lo(LATB)($3)
	.loc 1 465 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L117:
	.loc 1 469 0
	move	$2,$0
	j	.L95
	nop

.L84:
	.loc 1 475 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L118
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L119
	nop

.L118:
	.loc 1 477 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,7,1
	sh	$2,%lo(LATB)($3)
	.loc 1 478 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L119:
	.loc 1 482 0
	move	$2,$0
	j	.L95
	nop

.L85:
	.loc 1 488 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L120
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L121
	nop

.L120:
	.loc 1 490 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,8,1
	sh	$2,%lo(LATB)($3)
	.loc 1 491 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L121:
	.loc 1 495 0
	move	$2,$0
	j	.L95
	nop

.L86:
	.loc 1 501 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L122
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L123
	nop

.L122:
	.loc 1 503 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,9,1
	sh	$2,%lo(LATB)($3)
	.loc 1 504 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L123:
	.loc 1 508 0
	move	$2,$0
	j	.L95
	nop

.L87:
	.loc 1 514 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L124
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L125
	nop

.L124:
	.loc 1 516 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,10,1
	sh	$2,%lo(LATB)($3)
	.loc 1 517 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L125:
	.loc 1 521 0
	move	$2,$0
	j	.L95
	nop

.L88:
	.loc 1 527 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L126
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L127
	nop

.L126:
	.loc 1 529 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,11,1
	sh	$2,%lo(LATB)($3)
	.loc 1 530 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L127:
	.loc 1 534 0
	move	$2,$0
	j	.L95
	nop

.L89:
	.loc 1 540 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L128
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L129
	nop

.L128:
	.loc 1 542 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,12,1
	sh	$2,%lo(LATB)($3)
	.loc 1 543 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L129:
	.loc 1 547 0
	move	$2,$0
	j	.L95
	nop

.L90:
	.loc 1 553 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L130
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L131
	nop

.L130:
	.loc 1 555 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,13,1
	sh	$2,%lo(LATB)($3)
	.loc 1 556 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L131:
	.loc 1 560 0
	move	$2,$0
	j	.L95
	nop

.L91:
	.loc 1 566 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L132
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L133
	nop

.L132:
	.loc 1 568 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,14,1
	sh	$2,%lo(LATB)($3)
	.loc 1 569 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L133:
	.loc 1 573 0
	move	$2,$0
	j	.L95
	nop

.L92:
	.loc 1 579 0
	lw	$3,12($fp)
	li	$2,1			# 0x1
	beq	$3,$2,.L134
	nop

	lw	$2,12($fp)
	bne	$2,$0,.L135
	nop

.L134:
	.loc 1 581 0
	lw	$2,12($fp)
	andi	$2,$2,0x00ff
	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,15,1
	sh	$2,%lo(LATB)($3)
	.loc 1 582 0
	li	$2,1			# 0x1
	j	.L95
	nop

.L135:
	.loc 1 586 0
	move	$2,$0
	j	.L95
	nop

.L70:
	.loc 1 590 0
	move	$2,$0
.L95:
	.loc 1 593 0
	move	$sp,$fp
.LCFI13 = .
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
.LCFI14 = .
	j	$31
	nop

	.set	macro
	.set	reorder
# Begin mchp_output_function_epilogue
# End mchp_output_function_epilogue
	.end	digitalWrite
.LFE9:
	.size	digitalWrite, .-digitalWrite
	.align	2
	.globl	digitalRead
.LFB10 = .
	.loc 1 601 0
	.set	nomips16
	.set	nomicromips
	.ent	digitalRead
	.type	digitalRead, @function
digitalRead:
	.frame	$fp,16,$31		# vars= 8, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
# End mchp_output_function_prologue
	addiu	$sp,$sp,-16
.LCFI15 = .
	sw	$fp,12($sp)
.LCFI16 = .
	move	$fp,$sp
.LCFI17 = .
	sw	$4,16($fp)
	.loc 1 605 0
	lw	$2,16($fp)
	sltu	$2,$2,22
	beq	$2,$0,.L137
	nop

	lw	$2,16($fp)
	sll	$3,$2,2
	lui	$2,%hi(.L139)
	addiu	$2,$2,%lo(.L139)
	addu	$2,$3,$2
	lw	$2,0($2)
	j	$2
	nop

	.align	2
	.align	2
.L139:
	.word	.L137
	.word	.L138
	.word	.L140
	.word	.L141
	.word	.L142
	.word	.L143
	.word	.L144
	.word	.L145
	.word	.L146
	.word	.L147
	.word	.L148
	.word	.L149
	.word	.L150
	.word	.L151
	.word	.L152
	.word	.L153
	.word	.L154
	.word	.L155
	.word	.L156
	.word	.L157
	.word	.L158
	.word	.L159
.L138:
	.loc 1 609 0
	lui	$2,%hi(PORTA)
	lw	$2,%lo(PORTA)($2)
	ext	$2,$2,0,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 610 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L140:
	.loc 1 614 0
	lui	$2,%hi(PORTA)
	lw	$2,%lo(PORTA)($2)
	ext	$2,$2,1,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 615 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L141:
	.loc 1 619 0
	lui	$2,%hi(PORTA)
	lw	$2,%lo(PORTA)($2)
	ext	$2,$2,2,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 620 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L142:
	.loc 1 624 0
	lui	$2,%hi(PORTA)
	lw	$2,%lo(PORTA)($2)
	ext	$2,$2,3,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 625 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L143:
	.loc 1 629 0
	lui	$2,%hi(PORTA)
	lw	$2,%lo(PORTA)($2)
	ext	$2,$2,4,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 631 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L144:
	.loc 1 635 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,0,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 637 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L145:
	.loc 1 641 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,1,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 643 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L146:
	.loc 1 647 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,2,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 649 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L147:
	.loc 1 653 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,3,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 655 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L148:
	.loc 1 659 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,4,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 661 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L149:
	.loc 1 665 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,5,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 667 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L150:
	.loc 1 671 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,6,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 673 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L151:
	.loc 1 677 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,7,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 679 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L152:
	.loc 1 683 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,8,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 685 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L153:
	.loc 1 689 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,9,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 691 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L154:
	.loc 1 695 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,10,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 697 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L155:
	.loc 1 701 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,11,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 703 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L156:
	.loc 1 707 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,12,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 709 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L157:
	.loc 1 713 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,13,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 715 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L158:
	.loc 1 719 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,14,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 721 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L159:
	.loc 1 725 0
	lui	$2,%hi(PORTB)
	lw	$2,%lo(PORTB)($2)
	ext	$2,$2,15,1
	andi	$2,$2,0x00ff
	sb	$2,0($fp)
	.loc 1 727 0
	lbu	$2,0($fp)
	j	.L160
	nop

.L137:
	.loc 1 730 0
	move	$2,$0
.L160:
	.loc 1 733 0
	move	$sp,$fp
.LCFI18 = .
	lw	$fp,12($sp)
	addiu	$sp,$sp,16
.LCFI19 = .
	j	$31
	nop

	.set	macro
	.set	reorder
# Begin mchp_output_function_epilogue
# End mchp_output_function_epilogue
	.end	digitalRead
.LFE10:
	.size	digitalRead, .-digitalRead
	.align	2
	.globl	main
.LFB11 = .
	.loc 1 746 0
	.set	nomips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$fp,24,$31		# vars= 0, regs= 2/0, args= 16, gp= 0
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
# End mchp_output_function_prologue
	addiu	$sp,$sp,-24
.LCFI20 = .
	sw	$31,20($sp)
	sw	$fp,16($sp)
.LCFI21 = .
	move	$fp,$sp
.LCFI22 = .
	.loc 1 748 0
	jal	digiPin
	nop

	.loc 1 754 0
	lui	$2,%hi(TRISA)
	li	$3,1			# 0x1
	sw	$3,%lo(TRISA)($2)
	.loc 1 755 0
	lui	$2,%hi(TRISB)
	li	$3,1			# 0x1
	sw	$3,%lo(TRISB)($2)
	.loc 1 756 0
	li	$4,21			# 0x15
	move	$5,$0
	jal	pinMode
	nop

	.loc 1 757 0
	li	$4,1			# 0x1
	li	$5,1			# 0x1
	jal	pinMode
	nop

.L166:
.LBB2 = .
	.loc 1 766 0
	lui	$2,%hi(LATA)
	sw	$0,%lo(LATA)($2)
	.loc 1 767 0
	lui	$2,%hi(LATB)
	sw	$0,%lo(LATB)($2)
	.loc 1 772 0
	j	.L162
	nop

.L165:
	.loc 1 776 0
	lw	$2,%gp_rel(i.6950)($28)
	move	$4,$2
	jal	digitalRead
	nop

	andi	$2,$2,0x1
	andi	$4,$2,0x00ff
	lui	$3,%hi(LATB)
	lhu	$2,%lo(LATB)($3)
	ins	$2,$4,15,1
	sh	$2,%lo(LATB)($3)
	.loc 1 778 0
	lui	$2,%hi(PORTA)
	lw	$2,%lo(PORTA)($2)
	andi	$2,$2,0x1
	beq	$2,$0,.L162
	nop

	.loc 1 780 0
	lw	$2,%gp_rel(i.6950)($28)
	addiu	$2,$2,1
	sw	$2,%gp_rel(i.6950)($28)
	.loc 1 782 0
	li	$2,1			# 0x1
	sw	$2,%gp_rel(j.6951)($28)
	j	.L163
	nop

.L164:
	lw	$2,%gp_rel(j.6951)($28)
	addiu	$2,$2,1
	sw	$2,%gp_rel(j.6951)($28)
.L163:
	lw	$3,%gp_rel(j.6951)($28)
	li	$2,983040			# 0xf0000
	ori	$2,$2,0x4240
	slt	$2,$3,$2
	bne	$2,$0,.L164
	nop

.L162:
	.loc 1 772 0
	lw	$2,%gp_rel(i.6950)($28)
	slt	$2,$2,21
	bne	$2,$0,.L165
	nop

.LBE2 = .
	.loc 1 786 0
	j	.L166
	nop

	.set	macro
	.set	reorder
# Begin mchp_output_function_epilogue
# End mchp_output_function_epilogue
	.end	main
.LFE11:
	.size	main, .-main
	.section	.sdata,data
	.align	2
	.type	i.6950, @object
	.size	i.6950, 4
i.6950:
	.word	2
	.section	.sbss,bss
	.align	2
	.type	j.6951, @object
	.size	j.6951, 4
j.6951:
	.space	4
	.section	.debug_frame,info
.Lframe0:
	.4byte	.LECIE0-.LSCIE0
.LSCIE0:
	.4byte	0xffffffff
	.byte	0x1
	.ascii	"\000"
	.uleb128 0x1
	.sleb128 -4
	.byte	0x1f
	.byte	0xc
	.uleb128 0x1d
	.uleb128 0
	.align	2
.LECIE0:
.LSFDE0:
	.4byte	.LEFDE0-.LASFDE0
.LASFDE0:
	.4byte	.Lframe0
	.4byte	.LFB7
	.4byte	.LFE7-.LFB7
	.byte	0x4
	.4byte	.LCFI0-.LFB7
	.byte	0xe
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI1-.LCFI0
	.byte	0x9e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI2-.LCFI1
	.byte	0xd
	.uleb128 0x1e
	.byte	0x4
	.4byte	.LCFI3-.LCFI2
	.byte	0xd
	.uleb128 0x1d
	.byte	0x4
	.4byte	.LCFI4-.LCFI3
	.byte	0xe
	.uleb128 0
	.align	2
.LEFDE0:
.LSFDE2:
	.4byte	.LEFDE2-.LASFDE2
.LASFDE2:
	.4byte	.Lframe0
	.4byte	.LFB8
	.4byte	.LFE8-.LFB8
	.byte	0x4
	.4byte	.LCFI5-.LFB8
	.byte	0xe
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI6-.LCFI5
	.byte	0x9e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI7-.LCFI6
	.byte	0xd
	.uleb128 0x1e
	.byte	0x4
	.4byte	.LCFI8-.LCFI7
	.byte	0xd
	.uleb128 0x1d
	.byte	0x4
	.4byte	.LCFI9-.LCFI8
	.byte	0xe
	.uleb128 0
	.align	2
.LEFDE2:
.LSFDE4:
	.4byte	.LEFDE4-.LASFDE4
.LASFDE4:
	.4byte	.Lframe0
	.4byte	.LFB9
	.4byte	.LFE9-.LFB9
	.byte	0x4
	.4byte	.LCFI10-.LFB9
	.byte	0xe
	.uleb128 0x8
	.byte	0x4
	.4byte	.LCFI11-.LCFI10
	.byte	0x9e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI12-.LCFI11
	.byte	0xd
	.uleb128 0x1e
	.byte	0x4
	.4byte	.LCFI13-.LCFI12
	.byte	0xd
	.uleb128 0x1d
	.byte	0x4
	.4byte	.LCFI14-.LCFI13
	.byte	0xe
	.uleb128 0
	.align	2
.LEFDE4:
.LSFDE6:
	.4byte	.LEFDE6-.LASFDE6
.LASFDE6:
	.4byte	.Lframe0
	.4byte	.LFB10
	.4byte	.LFE10-.LFB10
	.byte	0x4
	.4byte	.LCFI15-.LFB10
	.byte	0xe
	.uleb128 0x10
	.byte	0x4
	.4byte	.LCFI16-.LCFI15
	.byte	0x9e
	.uleb128 0x1
	.byte	0x4
	.4byte	.LCFI17-.LCFI16
	.byte	0xd
	.uleb128 0x1e
	.byte	0x4
	.4byte	.LCFI18-.LCFI17
	.byte	0xd
	.uleb128 0x1d
	.byte	0x4
	.4byte	.LCFI19-.LCFI18
	.byte	0xe
	.uleb128 0
	.align	2
.LEFDE6:
.LSFDE8:
	.4byte	.LEFDE8-.LASFDE8
.LASFDE8:
	.4byte	.Lframe0
	.4byte	.LFB11
	.4byte	.LFE11-.LFB11
	.byte	0x4
	.4byte	.LCFI20-.LFB11
	.byte	0xe
	.uleb128 0x18
	.byte	0x4
	.4byte	.LCFI21-.LCFI20
	.byte	0x9f
	.uleb128 0x1
	.byte	0x9e
	.uleb128 0x2
	.byte	0x4
	.4byte	.LCFI22-.LCFI21
	.byte	0xd
	.uleb128 0x1e
	.align	2
.LEFDE8:
	.section	.text,code
.Letext0:
	.file 2 "c:/program files/microchip/xc32/v2.41/pic32mx/include/lega-c/machine/int_types.h"
	.file 3 "c:/program files (x86)/microchip/mplabx/v5.40/packs/microchip/pic32mx_dfp/1.3.231/include/proc/p32mx170f256b.h"
	.file 4 "c:/users/chris/documents/stanford university/trimester classes/20-21 autuum/me 218a/lab/lab 1/code/lab1proj.x/portmap.h"
	.section	.debug_info,info
.Ldebug_info0:
	.4byte	0xb62
	.2byte	0x2
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.uleb128 0x1
	.ascii	"GNU C 4.8.3 MPLAB XC32 Compiler v2.41\000"
	.byte	0x1
	.ascii	"portmod.c\000"
	.ascii	"C:/Users/chris/Documents/STANFORD UNIVERSITY/TRIMESTER C"
	.ascii	"LASSES/20-21 AUTUUM/ME 218A/LAB/LAB 1/CODE/Lab1Proj.X\000"
	.4byte	.Ltext0
	.4byte	.Letext0
	.4byte	.Ldebug_line0
	.uleb128 0x2
	.byte	0x1
	.byte	0x6
	.ascii	"signed char\000"
	.uleb128 0x3
	.ascii	"__uint8_t\000"
	.byte	0x2
	.byte	0x2f
	.4byte	0xd7
	.uleb128 0x2
	.byte	0x1
	.byte	0x8
	.ascii	"unsigned char\000"
	.uleb128 0x2
	.byte	0x2
	.byte	0x5
	.ascii	"short int\000"
	.uleb128 0x2
	.byte	0x2
	.byte	0x7
	.ascii	"short unsigned int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.ascii	"int\000"
	.uleb128 0x3
	.ascii	"__uint32_t\000"
	.byte	0x2
	.byte	0x33
	.4byte	0x124
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.ascii	"unsigned int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x5
	.ascii	"long long int\000"
	.uleb128 0x2
	.byte	0x8
	.byte	0x7
	.ascii	"long long unsigned int\000"
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x122a
	.4byte	0x1d2
	.uleb128 0x5
	.ascii	"TRISA0\000"
	.byte	0x3
	.2byte	0x122b
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISA1\000"
	.byte	0x3
	.2byte	0x122c
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISA2\000"
	.byte	0x3
	.2byte	0x122d
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISA3\000"
	.byte	0x3
	.2byte	0x122e
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISA4\000"
	.byte	0x3
	.2byte	0x122f
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x1231
	.4byte	0x1ec
	.uleb128 0x5
	.ascii	"w\000"
	.byte	0x3
	.2byte	0x1232
	.4byte	0x112
	.byte	0x4
	.byte	0x20
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.byte	0x3
	.2byte	0x1229
	.4byte	0x200
	.uleb128 0x7
	.4byte	0x15f
	.uleb128 0x7
	.4byte	0x1d2
	.byte	0
	.uleb128 0x8
	.ascii	"__TRISAbits_t\000"
	.byte	0x3
	.2byte	0x1234
	.4byte	0x1ec
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x123c
	.4byte	0x27a
	.uleb128 0x5
	.ascii	"RA0\000"
	.byte	0x3
	.2byte	0x123d
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RA1\000"
	.byte	0x3
	.2byte	0x123e
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RA2\000"
	.byte	0x3
	.2byte	0x123f
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RA3\000"
	.byte	0x3
	.2byte	0x1240
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RA4\000"
	.byte	0x3
	.2byte	0x1241
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x1243
	.4byte	0x294
	.uleb128 0x5
	.ascii	"w\000"
	.byte	0x3
	.2byte	0x1244
	.4byte	0x112
	.byte	0x4
	.byte	0x20
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.byte	0x3
	.2byte	0x123b
	.4byte	0x2a8
	.uleb128 0x7
	.4byte	0x216
	.uleb128 0x7
	.4byte	0x27a
	.byte	0
	.uleb128 0x8
	.ascii	"__PORTAbits_t\000"
	.byte	0x3
	.2byte	0x1246
	.4byte	0x294
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x124e
	.4byte	0x32c
	.uleb128 0x5
	.ascii	"LATA0\000"
	.byte	0x3
	.2byte	0x124f
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATA1\000"
	.byte	0x3
	.2byte	0x1250
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATA2\000"
	.byte	0x3
	.2byte	0x1251
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATA3\000"
	.byte	0x3
	.2byte	0x1252
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATA4\000"
	.byte	0x3
	.2byte	0x1253
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x1255
	.4byte	0x346
	.uleb128 0x5
	.ascii	"w\000"
	.byte	0x3
	.2byte	0x1256
	.4byte	0x112
	.byte	0x4
	.byte	0x20
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.byte	0x3
	.2byte	0x124d
	.4byte	0x35a
	.uleb128 0x7
	.4byte	0x2be
	.uleb128 0x7
	.4byte	0x32c
	.byte	0
	.uleb128 0x8
	.ascii	"__LATAbits_t\000"
	.byte	0x3
	.2byte	0x1258
	.4byte	0x346
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x12e1
	.4byte	0x4cf
	.uleb128 0x5
	.ascii	"TRISB0\000"
	.byte	0x3
	.2byte	0x12e2
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB1\000"
	.byte	0x3
	.2byte	0x12e3
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB2\000"
	.byte	0x3
	.2byte	0x12e4
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB3\000"
	.byte	0x3
	.2byte	0x12e5
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB4\000"
	.byte	0x3
	.2byte	0x12e6
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB5\000"
	.byte	0x3
	.2byte	0x12e7
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB6\000"
	.byte	0x3
	.2byte	0x12e8
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB7\000"
	.byte	0x3
	.2byte	0x12e9
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB8\000"
	.byte	0x3
	.2byte	0x12ea
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x17
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB9\000"
	.byte	0x3
	.2byte	0x12eb
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB10\000"
	.byte	0x3
	.2byte	0x12ec
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB11\000"
	.byte	0x3
	.2byte	0x12ed
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB12\000"
	.byte	0x3
	.2byte	0x12ee
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB13\000"
	.byte	0x3
	.2byte	0x12ef
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB14\000"
	.byte	0x3
	.2byte	0x12f0
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"TRISB15\000"
	.byte	0x3
	.2byte	0x12f1
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x12f3
	.4byte	0x4e9
	.uleb128 0x5
	.ascii	"w\000"
	.byte	0x3
	.2byte	0x12f4
	.4byte	0x112
	.byte	0x4
	.byte	0x20
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.byte	0x3
	.2byte	0x12e0
	.4byte	0x4fd
	.uleb128 0x7
	.4byte	0x36f
	.uleb128 0x7
	.4byte	0x4cf
	.byte	0
	.uleb128 0x8
	.ascii	"__TRISBbits_t\000"
	.byte	0x3
	.2byte	0x12f6
	.4byte	0x4e9
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x12fe
	.4byte	0x643
	.uleb128 0x5
	.ascii	"RB0\000"
	.byte	0x3
	.2byte	0x12ff
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB1\000"
	.byte	0x3
	.2byte	0x1300
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB2\000"
	.byte	0x3
	.2byte	0x1301
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB3\000"
	.byte	0x3
	.2byte	0x1302
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB4\000"
	.byte	0x3
	.2byte	0x1303
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB5\000"
	.byte	0x3
	.2byte	0x1304
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB6\000"
	.byte	0x3
	.2byte	0x1305
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB7\000"
	.byte	0x3
	.2byte	0x1306
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB8\000"
	.byte	0x3
	.2byte	0x1307
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x17
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB9\000"
	.byte	0x3
	.2byte	0x1308
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB10\000"
	.byte	0x3
	.2byte	0x1309
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB11\000"
	.byte	0x3
	.2byte	0x130a
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB12\000"
	.byte	0x3
	.2byte	0x130b
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB13\000"
	.byte	0x3
	.2byte	0x130c
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB14\000"
	.byte	0x3
	.2byte	0x130d
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"RB15\000"
	.byte	0x3
	.2byte	0x130e
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x1310
	.4byte	0x65d
	.uleb128 0x5
	.ascii	"w\000"
	.byte	0x3
	.2byte	0x1311
	.4byte	0x112
	.byte	0x4
	.byte	0x20
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.byte	0x3
	.2byte	0x12fd
	.4byte	0x671
	.uleb128 0x7
	.4byte	0x513
	.uleb128 0x7
	.4byte	0x643
	.byte	0
	.uleb128 0x8
	.ascii	"__PORTBbits_t\000"
	.byte	0x3
	.2byte	0x1313
	.4byte	0x65d
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x131b
	.4byte	0x7d7
	.uleb128 0x5
	.ascii	"LATB0\000"
	.byte	0x3
	.2byte	0x131c
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1f
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB1\000"
	.byte	0x3
	.2byte	0x131d
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1e
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB2\000"
	.byte	0x3
	.2byte	0x131e
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1d
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB3\000"
	.byte	0x3
	.2byte	0x131f
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1c
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB4\000"
	.byte	0x3
	.2byte	0x1320
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1b
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB5\000"
	.byte	0x3
	.2byte	0x1321
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x1a
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB6\000"
	.byte	0x3
	.2byte	0x1322
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x19
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB7\000"
	.byte	0x3
	.2byte	0x1323
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x18
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB8\000"
	.byte	0x3
	.2byte	0x1324
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x17
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB9\000"
	.byte	0x3
	.2byte	0x1325
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x16
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB10\000"
	.byte	0x3
	.2byte	0x1326
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x15
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB11\000"
	.byte	0x3
	.2byte	0x1327
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x14
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB12\000"
	.byte	0x3
	.2byte	0x1328
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x13
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB13\000"
	.byte	0x3
	.2byte	0x1329
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x12
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB14\000"
	.byte	0x3
	.2byte	0x132a
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x11
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.uleb128 0x5
	.ascii	"LATB15\000"
	.byte	0x3
	.2byte	0x132b
	.4byte	0x112
	.byte	0x4
	.byte	0x1
	.byte	0x10
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x4
	.byte	0x4
	.byte	0x3
	.2byte	0x132d
	.4byte	0x7f1
	.uleb128 0x5
	.ascii	"w\000"
	.byte	0x3
	.2byte	0x132e
	.4byte	0x112
	.byte	0x4
	.byte	0x20
	.byte	0
	.byte	0x2
	.byte	0x23
	.uleb128 0
	.byte	0
	.uleb128 0x6
	.byte	0x4
	.byte	0x3
	.2byte	0x131a
	.4byte	0x805
	.uleb128 0x7
	.4byte	0x687
	.uleb128 0x7
	.4byte	0x7d7
	.byte	0
	.uleb128 0x8
	.ascii	"__LATBbits_t\000"
	.byte	0x3
	.2byte	0x1330
	.4byte	0x7f1
	.uleb128 0x2
	.byte	0x4
	.byte	0x7
	.ascii	"long unsigned int\000"
	.uleb128 0x2
	.byte	0x4
	.byte	0x5
	.ascii	"long int\000"
	.uleb128 0x9
	.byte	0x4
	.byte	0x4
	.byte	0xa
	.4byte	0x8c8
	.uleb128 0xa
	.ascii	"RA0\000"
	.sleb128 1
	.uleb128 0xa
	.ascii	"RA1\000"
	.sleb128 2
	.uleb128 0xa
	.ascii	"RA2\000"
	.sleb128 3
	.uleb128 0xa
	.ascii	"RA3\000"
	.sleb128 4
	.uleb128 0xa
	.ascii	"RA4\000"
	.sleb128 5
	.uleb128 0xa
	.ascii	"RB0\000"
	.sleb128 6
	.uleb128 0xa
	.ascii	"RB1\000"
	.sleb128 7
	.uleb128 0xa
	.ascii	"RB2\000"
	.sleb128 8
	.uleb128 0xa
	.ascii	"RB3\000"
	.sleb128 9
	.uleb128 0xa
	.ascii	"RB4\000"
	.sleb128 10
	.uleb128 0xa
	.ascii	"RB5\000"
	.sleb128 11
	.uleb128 0xa
	.ascii	"RB6\000"
	.sleb128 12
	.uleb128 0xa
	.ascii	"RB7\000"
	.sleb128 13
	.uleb128 0xa
	.ascii	"RB8\000"
	.sleb128 14
	.uleb128 0xa
	.ascii	"RB9\000"
	.sleb128 15
	.uleb128 0xa
	.ascii	"RB10\000"
	.sleb128 16
	.uleb128 0xa
	.ascii	"RB11\000"
	.sleb128 17
	.uleb128 0xa
	.ascii	"RB12\000"
	.sleb128 18
	.uleb128 0xa
	.ascii	"RB13\000"
	.sleb128 19
	.uleb128 0xa
	.ascii	"RB14\000"
	.sleb128 20
	.uleb128 0xa
	.ascii	"RB15\000"
	.sleb128 21
	.byte	0
	.uleb128 0x3
	.ascii	"pin_t\000"
	.byte	0x4
	.byte	0x20
	.4byte	0x83b
	.uleb128 0x9
	.byte	0x4
	.byte	0x4
	.byte	0x23
	.4byte	0x8eb
	.uleb128 0xa
	.ascii	"LOW\000"
	.sleb128 0
	.uleb128 0xa
	.ascii	"HIGH\000"
	.sleb128 1
	.byte	0
	.uleb128 0x3
	.ascii	"HL_t\000"
	.byte	0x4
	.byte	0x26
	.4byte	0x8d5
	.uleb128 0x9
	.byte	0x4
	.byte	0x4
	.byte	0x29
	.4byte	0x911
	.uleb128 0xa
	.ascii	"OUTPUT\000"
	.sleb128 0
	.uleb128 0xa
	.ascii	"INPUT\000"
	.sleb128 1
	.byte	0
	.uleb128 0x3
	.ascii	"IO_t\000"
	.byte	0x4
	.byte	0x2c
	.4byte	0x8f7
	.uleb128 0xb
	.byte	0x1
	.ascii	"digiPin\000"
	.byte	0x1
	.byte	0x6
	.byte	0x1
	.4byte	.LFB7
	.4byte	.LFE7
	.byte	0x1
	.byte	0x6e
	.byte	0x1
	.uleb128 0xc
	.byte	0x1
	.ascii	"pinMode\000"
	.byte	0x1
	.byte	0x16
	.byte	0x1
	.4byte	0x971
	.4byte	.LFB8
	.4byte	.LFE8
	.byte	0x1
	.byte	0x6e
	.byte	0x1
	.4byte	0x971
	.uleb128 0xd
	.ascii	"pin\000"
	.byte	0x1
	.byte	0x16
	.4byte	0x8c8
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0xd
	.ascii	"IO\000"
	.byte	0x1
	.byte	0x16
	.4byte	0x911
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.byte	0
	.uleb128 0x2
	.byte	0x1
	.byte	0x2
	.ascii	"_Bool\000"
	.uleb128 0xe
	.byte	0x1
	.ascii	"digitalWrite\000"
	.byte	0x1
	.2byte	0x137
	.byte	0x1
	.4byte	0x971
	.4byte	.LFB9
	.4byte	.LFE9
	.byte	0x1
	.byte	0x6e
	.byte	0x1
	.4byte	0x9be
	.uleb128 0xf
	.ascii	"pin\000"
	.byte	0x1
	.2byte	0x137
	.4byte	0x8c8
	.byte	0x2
	.byte	0x91
	.sleb128 8
	.uleb128 0xf
	.ascii	"HL\000"
	.byte	0x1
	.2byte	0x137
	.4byte	0x8eb
	.byte	0x2
	.byte	0x91
	.sleb128 12
	.byte	0
	.uleb128 0xe
	.byte	0x1
	.ascii	"digitalRead\000"
	.byte	0x1
	.2byte	0x258
	.byte	0x1
	.4byte	0xc6
	.4byte	.LFB10
	.4byte	.LFE10
	.byte	0x1
	.byte	0x6e
	.byte	0x1
	.4byte	0xa01
	.uleb128 0xf
	.ascii	"pin\000"
	.byte	0x1
	.2byte	0x258
	.4byte	0x8c8
	.byte	0x2
	.byte	0x91
	.sleb128 16
	.uleb128 0x10
	.ascii	"dR\000"
	.byte	0x1
	.2byte	0x25b
	.4byte	0xc6
	.byte	0x2
	.byte	0x91
	.sleb128 0
	.byte	0
	.uleb128 0x11
	.byte	0x1
	.ascii	"main\000"
	.byte	0x1
	.2byte	0x2e9
	.byte	0x1
	.4byte	0x10b
	.4byte	.LFB11
	.4byte	.LFE11
	.byte	0x1
	.byte	0x6e
	.byte	0x1
	.4byte	0xa4a
	.uleb128 0x12
	.4byte	.LBB2
	.4byte	.LBE2
	.uleb128 0x10
	.ascii	"i\000"
	.byte	0x1
	.2byte	0x301
	.4byte	0x10b
	.byte	0x5
	.byte	0x3
	.4byte	i.6950
	.uleb128 0x10
	.ascii	"j\000"
	.byte	0x1
	.2byte	0x302
	.4byte	0x10b
	.byte	0x5
	.byte	0x3
	.4byte	j.6951
	.byte	0
	.byte	0
	.uleb128 0x13
	.ascii	"ANSELA\000"
	.byte	0x3
	.2byte	0x1219
	.4byte	0xa5b
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	0x112
	.uleb128 0x13
	.ascii	"TRISA\000"
	.byte	0x3
	.2byte	0x1228
	.4byte	0xa5b
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.ascii	"TRISAbits\000"
	.byte	0x3
	.2byte	0x1235
	.ascii	"TRISA\000"
	.4byte	0xa8a
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	0x200
	.uleb128 0x15
	.ascii	"PORTAbits\000"
	.byte	0x3
	.2byte	0x1247
	.ascii	"PORTA\000"
	.4byte	0xaa9
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	0x2a8
	.uleb128 0x13
	.ascii	"LATA\000"
	.byte	0x3
	.2byte	0x124c
	.4byte	0xa5b
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.ascii	"LATAbits\000"
	.byte	0x3
	.2byte	0x1259
	.ascii	"LATA\000"
	.4byte	0xad5
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	0x35a
	.uleb128 0x13
	.ascii	"ANSELB\000"
	.byte	0x3
	.2byte	0x12c9
	.4byte	0xa5b
	.byte	0x1
	.byte	0x1
	.uleb128 0x13
	.ascii	"TRISB\000"
	.byte	0x3
	.2byte	0x12df
	.4byte	0xa5b
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.ascii	"TRISBbits\000"
	.byte	0x3
	.2byte	0x12f7
	.ascii	"TRISB\000"
	.4byte	0xb15
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	0x4fd
	.uleb128 0x15
	.ascii	"PORTBbits\000"
	.byte	0x3
	.2byte	0x1314
	.ascii	"PORTB\000"
	.4byte	0xb34
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	0x671
	.uleb128 0x13
	.ascii	"LATB\000"
	.byte	0x3
	.2byte	0x1319
	.4byte	0xa5b
	.byte	0x1
	.byte	0x1
	.uleb128 0x15
	.ascii	"LATBbits\000"
	.byte	0x3
	.2byte	0x1331
	.ascii	"LATB\000"
	.4byte	0xb60
	.byte	0x1
	.byte	0x1
	.uleb128 0x14
	.4byte	0x805
	.byte	0
	.section	.debug_abbrev,info
.Ldebug_abbrev0:
	.uleb128 0x1
	.uleb128 0x11
	.byte	0x1
	.uleb128 0x25
	.uleb128 0x8
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x1b
	.uleb128 0x8
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x10
	.uleb128 0x6
	.byte	0
	.byte	0
	.uleb128 0x2
	.uleb128 0x24
	.byte	0
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3e
	.uleb128 0xb
	.uleb128 0x3
	.uleb128 0x8
	.byte	0
	.byte	0
	.uleb128 0x3
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x4
	.uleb128 0x13
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x5
	.uleb128 0xd
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0xd
	.uleb128 0xb
	.uleb128 0xc
	.uleb128 0xb
	.uleb128 0x38
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x6
	.uleb128 0x17
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x7
	.uleb128 0xd
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x8
	.uleb128 0x16
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x9
	.uleb128 0x4
	.byte	0x1
	.uleb128 0xb
	.uleb128 0xb
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xa
	.uleb128 0x28
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x1c
	.uleb128 0xd
	.byte	0
	.byte	0
	.uleb128 0xb
	.uleb128 0x2e
	.byte	0
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.uleb128 0x2117
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0xc
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.uleb128 0x2117
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xd
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0xb
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0xe
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.uleb128 0x2117
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0xf
	.uleb128 0x5
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x10
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x2
	.uleb128 0xa
	.byte	0
	.byte	0
	.uleb128 0x11
	.uleb128 0x2e
	.byte	0x1
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x27
	.uleb128 0xc
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.uleb128 0x40
	.uleb128 0xa
	.uleb128 0x2116
	.uleb128 0xc
	.uleb128 0x1
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x12
	.uleb128 0xb
	.byte	0x1
	.uleb128 0x11
	.uleb128 0x1
	.uleb128 0x12
	.uleb128 0x1
	.byte	0
	.byte	0
	.uleb128 0x13
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.uleb128 0x14
	.uleb128 0x35
	.byte	0
	.uleb128 0x49
	.uleb128 0x13
	.byte	0
	.byte	0
	.uleb128 0x15
	.uleb128 0x34
	.byte	0
	.uleb128 0x3
	.uleb128 0x8
	.uleb128 0x3a
	.uleb128 0xb
	.uleb128 0x3b
	.uleb128 0x5
	.uleb128 0x2007
	.uleb128 0x8
	.uleb128 0x49
	.uleb128 0x13
	.uleb128 0x3f
	.uleb128 0xc
	.uleb128 0x3c
	.uleb128 0xc
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,info
	.4byte	0x1c
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.Ltext0
	.4byte	.Letext0-.Ltext0
	.4byte	0
	.4byte	0
	.section	.debug_line,info
.Ldebug_line0:
	.section	.debug_str,info
	.ident	"GCC: (Microchip Technology) 4.8.3 MPLAB XC32 Compiler v2.41"
# Begin MCHP vector dispatch table
# End MCHP vector dispatch table
# Microchip Technology PIC32 MCU configuration words
# Configuration word @ 0xbfc00bfc
	.section	.config_BFC00BFC, code, keep, address(0xBFC00BFC)
	.type	__config_BFC00BFC, @object
	.size	__config_BFC00BFC, 4
__config_BFC00BFC:
	.word	0x7FFFFFFB
# Configuration word @ 0xbfc00bf8
	.section	.config_BFC00BF8, code, keep, address(0xBFC00BF8)
	.type	__config_BFC00BF8, @object
	.size	__config_BFC00BF8, 4
__config_BFC00BF8:
	.word	0xFF74DF59
# Configuration word @ 0xbfc00bf4
	.section	.config_BFC00BF4, code, keep, address(0xBFC00BF4)
	.type	__config_BFC00BF4, @object
	.size	__config_BFC00BF4, 4
__config_BFC00BF4:
	.word	0xFFF9FFD9
# Configuration word @ 0xbfc00bf0
	.section	.config_BFC00BF0, code, keep, address(0xBFC00BF0)
	.type	__config_BFC00BF0, @object
	.size	__config_BFC00BF0, 4
__config_BFC00BF0:
	.word	0xCFFFFFFF
