#ifndef PORTMAP_H
#define	PORTMAP_H

//Other header files 
#include <stdint.h>
#include <stdbool.h>

//TypeDef for mapping ports to integer pin
typedef enum
{
    RA0 = 1,
    RA1,
    RA2,
    RA3,
    RA4,
    RB0,
    RB1,
    RB2,
    RB3,
    RB4,
    RB5, 
    RB6, 
    RB7, 
    RB8, 
    RB9, 
    RB10, 
    RB11, 
    RB12, 
    RB13, 
    RB14, 
    RB15,    
}pin_t;

typedef enum
{
   LOW = 0,
   HIGH = 1,
}HL_t;

typedef enum
{
   OUTPUT = 0,
   INPUT = 1,
}IO_t;

void digiPin(void);
bool pinMode(pin_t pin, IO_t IO);
bool digitalWrite(pin_t pin, HL_t HL);
uint8_t digitalRead(pin_t pin);

#endif	/* PORTMAP_H */

