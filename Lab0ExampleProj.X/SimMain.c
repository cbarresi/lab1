
#include "ME218_PIC32_2020.h"
#include <xc.h>


//	Function Prototypes
int main(void);



#define LED1        LATBbits.LATB2
#define LED2        LATBbits.LATB3
#define SW1         PORTAbits.RA2
#define SW2         PORTAbits.RA3


int main(void) {
    /*Configure tri-state registers*/
    TRISAbits.TRISA2 = 1;   //SW1 as input
    TRISAbits.TRISA3 = 1;   //SW2 as input
     
    TRISBbits.TRISB2 = 0;   //LED1 as output
    TRISBbits.TRISB3 = 0;   //LED2 as output
     
    while(1)
    {
        LED1 = !SW1;    //LED1 turns on when SW1 is pressed
        
        //Modification for lab 1 part 2 
        LED2 = 1;
        LED2 = 0;
    }

}



